#!/usr/bin/env python
import time
import sys
import serial
import os
print ('Scan time:'+sys.argv[1])
sleepTime = int(sys.argv[1])
COM_PORT = "/dev/ttyACM0"
BAUD_RATES = 115200
time.sleep(3)
hasStarted = False
retryCount = 0

def startTimeoutLidar():
    global retryCount
    global hasStarted
    retryCount = retryCount+1
    ser = serial.Serial(COM_PORT,BAUD_RATES,timeout=2)
    print(ser)
    while ser.isOpen() == False:
        time.sleep(5)
        print("port to arduino is not yet opened")
    arduinoData = ser.readline()
    ser.write(b'0\n')
    time.sleep(5)
    ser.write(b'1\n')
    time.sleep(sleepTime+10)
    ser.write(b'0\n')

        

while hasStarted==False and retryCount<20:
    try:
        hasStarted = True
        if sleepTime>0:
            startTimeoutLidar()
    except Exception as e:
        print(e)
        time.sleep(5)
        print('Not able to start up lidar in live stream')
    

        

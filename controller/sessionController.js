const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("displacement.db");

function auth(req, res) {
  var username = req.body.name;
  var password = req.body.pass;
  let jsonMsg = {};
  if (username && password) {
    db.serialize(() => {
      db.all(
        "SELECT PASSWORD FROM ACCOUNT WHERE NAME = ?;",
        [username],
        (err, row) => {
          let dbCurrentPassword = row[0].password;
          if (password == dbCurrentPassword) {
            req.session.loggedin = true;
            req.session.username = username;
            jsonMsg.msg = "ok";
            jsonMsg.code = 200;
            res.send(jsonMsg);
          } else {
            jsonMsg.msg = "帳號密碼輸入錯誤";
            jsonMsg.code = 401;
            res.send(jsonMsg);
          }
          res.end();
        }
      );
    });
  } else {
    jsonMsg.msg = "請輸入帳號與密碼";
    jsonMsg.code = 403;
    res.send(jsonMsg);
    res.end();
  }
}

function destorySession(req, res) {
  let jsonMsg = {
    msg: "登出成功",
    code: 200,
  };
  try {
    res.clearCookie("session-id");
    req.session.destroy();
    res.send(jsonMsg);
  } catch (err) {
    jsonMsg.msg = err.message;
    res.send(jsonMsg);
  }
}

var checkLoginRequest = function (req, res) {
  let jsonMsg = {
    msg: "認證成功",
    code: 200,
  };
  try {
    if (!req.session || !req.session.loggedin) {
      jsonMsg.msg = "認證失敗";
      jsonMsg.code = 401;
      res.status(401).send(jsonMsg);
    } else {
      res.send(jsonMsg);
    }
  } catch (err) {
    jsonMsg.msg = err.message;
    res.status(401).send(jsonMsg);
  }
};

var checkLogin = function (req) {
  if (req.session && req.session.loggedin) {
    extendSession(req);
    return true;
  }
  return false;
};

function extendSession(req) {
  req.session._garbage = Date();
  req.session.touch();
}

module.exports = {
  auth,
  destorySession,
  checkLogin,
  checkLoginRequest,
};

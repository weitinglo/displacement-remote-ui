
const checkDiskSpace = require('check-disk-space').default

function storage(req, res) {
  var jsonRes = {
    code: 200,
    msg: "success",
    data: ""
};

checkDiskSpace('/mnt/').then((diskSpace) => {
    console.log(diskSpace)
    let info ={
        totalStorage:diskSpace.size,
        freeStorage:diskSpace.free
    }
    jsonRes.data = info;
    res.send(jsonRes);
})
}

module.exports = {
  storage
};

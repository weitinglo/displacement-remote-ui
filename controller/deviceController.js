const pm2 = require("pm2");
const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("displacement.db");
require("dotenv").config();
const { exec } = require('child_process');
var ping = require('ping');

const rosbagPath = process.env.ROSBAG;
const ftpPath = process.env.FTP_PATH;
const lidarPowerPath = process.env.LIDAR_POWER_PATH;
const startLiveStreamPath = process.env.START_LIVE_STREAM_PATH;
const endLiveStreamPath = process.env.END_LIVE_STREAM_PATH;

let networkInterval = null;
let syncInterval = null;
function restartAllPm2Process(scanFrequency, scanTime, type,lidarFilename,scanPeriodFilenamePrefix) {
  if(!lidarFilename){ 
    let nowDate = new Date();
    let currentDatetime = nowDate.getFullYear()+"-"+String(nowDate.getMonth()+1).padStart(2,'0')+"-"+String(nowDate.getDate()).padStart(2,'0')+"-"+String(nowDate.getHours()).padStart(2,'0')+"-"+String(nowDate.getMinutes()).padStart(2,'0')+"-"+String(nowDate.getSeconds()).padStart(2,'0')
    lidarFilename = currentDatetime;
  }
  pm2.describe("lidar-power", (err, data) => {
    let = powerConfig = {
      script: "lidar-power.py",
      name: "lidar-power",
      cwd: lidarPowerPath,
      autorestart: false,
      cron_restart: "0",
      interpreter: "python3",
      args: [scanTime],
    };
    let = startRecordConfig = {
    };
    let = stopRecordConfig = {
      script: "stop_bag_record.sh",
      name: "stop_bag_record",
      cwd: rosbagPath,
      autorestart: false,
      cron_restart: "0",
      interpreter: "bash",
      args: [scanTime],
    };
    if (type == "instant") { //即時掃描
      startRecordConfig = {
        script: "start_bag_record.sh",
        name: "start_bag_record",
        cwd: rosbagPath,
        autorestart: false,
        cron_restart: "0",
        interpreter: "bash",
        args: ["1",lidarFilename,""],
      };
      powerConfig.cron_restart = "5 */" + scanFrequency + " * * *";
      startRecordConfig.cron_restart = "5 */" + scanFrequency + " * * *";
      stopRecordConfig.cron_restart = "5 */" + scanFrequency + " * * *";
    }else{ //週期掃描
      startRecordConfig = {
        script: "start_bag_record.sh",
        name: "start_bag_record",
        cwd: rosbagPath,
        autorestart: false,
        cron_restart: "0",
        interpreter: "bash",
        args: ["2","",scanPeriodFilenamePrefix==null?"":scanPeriodFilenamePrefix],
      };
    }
    if (scanFrequency == 0) {
      pm2.delete("lidar-power");
      pm2.delete("start_bag_record");
      pm2.delete("stop_bag_record");
    } else {
      if (data && data[0] && data[0]["pm2_env"]) {
        let processStatus = data[0]["pm2_env"].status;
        pm2.restart(powerConfig, function (errPower, apps) {
          // if (errPower) {
          //   return pm2.disconnect();
          // }
        });
        pm2.restart(startRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });

        pm2.restart(stopRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
      } else {
        //還沒有執行過
        pm2.start(powerConfig, function (errPower, apps) {
          // if (errPower) {
          //   return pm2.disconnect();
          // }
        });
        pm2.start(startRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
        pm2.start(stopRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
      }

      //一執行完後馬上上傳資料
     // setTimeout(startToCheckInternet,310*1000)
    }
  });
}

function liveDisplayPm2Process(enable) {
  pm2.delete("lidar-power");
  pm2.delete("end-live-stream");
  pm2.delete("start-live-stream");
  pm2.delete("start_bag_record");
  pm2.delete("stop_bag_record");
  let = startLiveStreamConfig = {
    script: "start-live-stream.py",
    name: "start-live-stream",
    cwd: startLiveStreamPath,
    autorestart: false,
    interpreter: "python3",
    restart_delay: 5000,
    max_restarts: 10
  };
  let = endLiveStreamConfig = {
    script: "end-live-stream.py",
    name: "end-live-stream",
    cwd: endLiveStreamPath,
    autorestart: false,
    interpreter: "python3",
    restart_delay: 5000,
    max_restarts: 10
  };
  pm2.describe("start-live-stream", (startErr, startData) => {
    let startLiveStreamProcessStatus = "";
    if (startData && startData[0] && startData[0]["pm2_env"]) 
      startLiveStreamProcessStatus = startData[0]["pm2_env"].status; //取得 start live stream pm2 狀態
    pm2.describe("end-live-stream", (endErr, endData) => {
      let endLiveStreamProcessStatus = "";
      if (endData && endData[0] && endData[0]["pm2_env"]) 
        endLiveStreamProcessStatus = endData[0]["pm2_env"].status;  //取得 end live stream pm2 狀態
      //邏輯
      if(enable){//開始畫面顯示
        if(startLiveStreamProcessStatus==""){ //尚未執行
          pm2.start(startLiveStreamConfig);
        }
      }else{//停止畫面顯示
        if(endLiveStreamProcessStatus==""){ //尚未執行
          pm2.start(endLiveStreamConfig);
        }
      }
    });
  });
}
function startToCheckInternet(){
  networkInterval  = setInterval(checkInternet,30*1000); //check internet every 20 secs

}
function checkInternet(){
  let host = "192.168.96.1";
  ping.sys.probe(host, function(isAlive){
    if(isAlive){
      clearInterval(networkInterval);
      setTimeout(syncData,30*1000);
    }
  });
}
function shutDown(){
//  exec('sudo shutdown -h now');
}

function checkSyncStats(){
  pm2.describe("ftp-up", (err, data) => {
    if (data && data[0] && data[0]["pm2_env"]) {
      let processStatus = data[0]["pm2_env"].status;
      if (processStatus != "online") { //上傳完畢
        // clearInterval(syncInterval);
        // setTimeout(shutDown,2*60*1000) 
      }
    }
  });
}
function syncData(){
  
  pm2.describe("ftp-up", (err, data) => {
    if (data && data[0] && data[0]["pm2_env"]) {
      let processStatus = data[0]["pm2_env"].status;
      if (processStatus != "online") {
        pm2.restart(
          {
            script: "ftp-up.sh",
            name: "ftp-up",
            cwd: ftpPath,
            autorestart: false,
            interpreter: "bash",
          },
          function (errPower, apps) {
            syncInterval = setInterval(checkSyncStats,10000);
          }
        );
      }
    } else {
      //還沒有執行過
      //console.log("Sync has not yet been started");
      pm2.start(
        {
          script: "ftp-up.sh",
          name: "ftp-up",
          cwd: ftpPath,
          autorestart: false,
          interpreter: "bash",
        },
        function (errPower, apps) {
          syncInterval = setInterval(checkSyncStats,10000);

        }
      );
    }
  });
}

function initSetting() {
  db.serialize(() => {
    db.all("SELECT SCAN_FREQUENCY, SCAN_TIME,SCAN_PERIOD_FILENAME FROM SETTING WHERE ID =1;", (err, row) => {
      let scanFrequency = row[0].scan_frequency;
      let scanTime = row[0].scan_time;
      let scanPeriodFilename = row[0].scan_period_filename;
      let type = "";
      if (scanFrequency == 0) type = "instant";
      restartAllPm2Process(scanFrequency, scanTime, type, "",scanPeriodFilename);
    });
  });
}

// function initSetting() {
//   restartAllPm2Process(0, 300, "instant");

//   // db.serialize(() => {
//   //   db.all("SELECT SCAN_FREQUENCY, SCAN_TIME FROM SETTING WHERE ID =1;", (err, row) => {
//   //     let scanFrequency = row[0].scan_frequency;
//   //     let scanTime = row[0].scan_time;
//   //     let type = "";
//   //     if (scanFrequency == 0) type = "instant";
//   //   });
//   // });
// }

function liveDisplay(req,res){
  let jsonMsg = {
    msg: "掃描中",
    status: "",
    code: 200,
  };

  const enable = req.body.enable;
  disablePeriodic();
  liveDisplayPm2Process(enable);
  res.send(jsonMsg);
}

function deleteAllPm2Process(){
  pm2.delete("lidar-power");
  pm2.delete("end-live-stream");
  pm2.delete("start-live-stream");
  pm2.delete("start_bag_record");
  pm2.delete("stop_bag_record");
}
function scan(req, res) {
  deleteAllPm2Process();
  let jsonMsg = {
    msg: "掃描中",
    status: "",
    code: 200,
  };

  var setting = req.body.setting;
  const scanTime = setting.scantime;
  const lidarFilename = setting.lidarFilename;

  disablePeriodic();
  restartAllPm2Process(1, scanTime, "instant",lidarFilename,"");
  res.send(jsonMsg);
}
function disablePeriodic() {
  db.serialize(() => {
    const stmt = db.prepare("UPDATE SETTING SET SCAN_FREQUENCY = ? WHERE ID =1;");
    stmt.run(0);
    stmt.finalize();
  });
}

function deviceStatus(req, res) {
  pm2.describe("lidar-power", (err, data) => {
    pm2.describe("start-live-stream", (startLiveStreamErr, startLiveStreamData) => {
      let jsonMsg = {
        msg: "閒置中",
        status: "",
        code: 200,
      };
      if (data && data[0] && data[0]["pm2_env"]) {
        let processStatus = data[0]["pm2_env"].status;
        if (processStatus == "online") {
          jsonMsg.status = processStatus;
          jsonMsg.msg = "運行中";
        }
      }else if(startLiveStreamData && startLiveStreamData[0] && startLiveStreamData[0]["pm2_env"]) {
        let processStatus = startLiveStreamData[0]["pm2_env"].status;
        jsonMsg.status = processStatus;
        jsonMsg.msg = "Live Stream 運行中";
        
      }
      res.send(jsonMsg);
    });
  });
}

function sync(req, res) {
  let jsonMsg = {
    msg: "閒置中",
    status: "",
    code: 200,
  };
  db.serialize(() => {
    const stmt = db.prepare(
      "UPDATE SETTING SET SYNC_DATETIME = ? WHERE ID =1;"
    );
    let nowDate = new Date();
    let currentDatetime = nowDate.getFullYear()+"-"+String(nowDate.getMonth()+1).padStart(2,'0')+"-"+String(nowDate.getDate()).padStart(2,'0')+" "+String(nowDate.getHours()).padStart(2,'0')+":"+String(nowDate.getMinutes()).padStart(2,'0');
    stmt.run(currentDatetime);
    stmt.finalize();
  });

  pm2.describe("ftp-up", (err, data) => {
    if (data && data[0] && data[0]["pm2_env"]) {
      let processStatus = data[0]["pm2_env"].status;
      jsonMsg.status = processStatus;
      if (processStatus == "online") {
        //運行中
        jsonMsg.msg = "運行中";
      } else {
        jsonMsg.msg = "開始";
        pm2.restart(
          {
            script: "ftp-up.sh",
            name: "ftp-up",
            cwd: ftpPath,
            autorestart: false,
            interpreter: "bash",
          },
          function (errPower, apps) {
            // if (errPower) {
            //   return pm2.disconnect();
            // }
          }
        );
      }
    } else {
      //還沒有執行過
     // console.log("Sync has not yet been started");
     // console.log(ftpPath)
      pm2.start(
        {
          script: "ftp-up.sh",
          name: "ftp-up",
          cwd: ftpPath,
          autorestart: false,
          interpreter: "bash",
        },
        function (errPower, apps) {
          if (errPower) {
            return pm2.disconnect();
          }
        }
      );
    }

    res.send(jsonMsg);
  });
}

function syncStatus(req, res) {
  let jsonMsg = {
    msg: "閒置中",
    status: "",
    data: null,
    code: 200,
  };
  db.serialize(() => {
    db.all("SELECT SYNC_DATETIME FROM SETTING WHERE ID =1;", (err, row) => {
      let sync_datetime = row[0].sync_datetime;
      jsonMsg.code = 200;
      jsonMsg.sync_datetime = sync_datetime;
      pm2.describe("ftp-up", (err, data) => {
        if (data && data[0] && data[0]["pm2_env"]) {
          let processStatus = data[0]["pm2_env"].status;
          jsonMsg.status = processStatus;
          if (processStatus == "online") {
            jsonMsg.msg = "運行中";
          }
        }
        res.send(jsonMsg);
      });
    });
  });
}


module.exports = {
  scan,
  deviceStatus,
  sync,
  syncStatus,
  liveDisplay,
  initSetting,
};

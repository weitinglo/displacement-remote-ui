const path = require("path");
const fs = require("fs");
const ftpPath = process.env.FTP_PATH;
require("dotenv").config();

function listFiles(req, res) {
  let jsonMsg = {
    msg: "",
    code: 200,
  };
  const directoryPath = path.join(ftpPath, "/FTP");
  //passsing directoryPath and callback function
  let response = [];
  const files = fs.readdirSync(directoryPath, 'utf8');
  for (let file of files) {
    const extension = path.extname(file);
    const fileSizeInBytes = fs.statSync(directoryPath +"/"+ file).size;
    response.push({ name: file, fileSizeInBytes });
  }
  jsonMsg.data = response;
  res.send(jsonMsg);

  // fs.readdir(directoryPath, function (err, files) {
  //   //handling error
  //   if (err) {
  //     return console.log("Unable to scan directory: " + err);
  //   }
  //   //listing all files using forEach
  //   files.forEach(function (file) {
  //     // Do whatever you want to do with the file
  //     console.log(file);
  //   });
  //   jsonMsg.data = files;
  //   res.send(jsonMsg);
  // });
}

function deleteFile(req, res) {
  let filename = req.query.filename;
  let jsonMsg = {
    msg: "",
    code: 200,
  };
  try {
    fs.unlinkSync(ftpPath + "/FTP/" + filename);
    jsonMsg.msg = "成功";
    res.send(jsonMsg);
  } catch (err) {
    console.error(err);
  }
}

module.exports = {
  listFiles,
  deleteFile,
};

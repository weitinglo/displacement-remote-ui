const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("displacement.db");
const pm2 = require("pm2");
require("dotenv").config();

const rosbagPath = process.env.ROSBAG;
const ftpPath = process.env.FTP_PATH;
const lidarPowerPath = process.env.LIDAR_POWER_PATH;

function getSetting(req, res) {
  let jsonMsg = {
    msg: "",
    data: {},
    code: 200,
  };
  db.serialize(() => {
    db.all("SELECT * FROM SETTING WHERE ID =1;", (err, row) => {
      jsonMsg.data = row[0];
      res.send(jsonMsg);
    });
  });
}


function restartAllPm2Process(scanFrequency, scanTime, type,scanPeriodFilenamePrefix) {
  pm2.describe("lidar-power", (err, data) => {
    let = powerConfig = {
      script: "lidar-power.py",
      name: "lidar-power",
      cwd: lidarPowerPath,
      autorestart: false,
      cron_restart: '0',
      interpreter: "python3",
      args: [scanTime],
    };
    let = startRecordConfig = {
      script: "start_bag_record.sh",
      name: "start_bag_record",
      cwd: rosbagPath,
      autorestart: false,
      cron_restart: '0',
      interpreter: "bash",
      args: ["2","",scanPeriodFilenamePrefix==null?"":scanPeriodFilenamePrefix],
    };
    let = stopRecordConfig = {
      script: "stop_bag_record.sh",
      name: "stop_bag_record",
      cwd: rosbagPath,
      autorestart: false,
      cron_restart: '0',
      interpreter: "bash",
      args: [scanTime],
    };
    if (type != "instant") {
      powerConfig.cron_restart = "0 */" + scanFrequency + " * * *";
      startRecordConfig.cron_restart = "0 */" + scanFrequency + " * * *";
      stopRecordConfig.cron_restart = "0 */" + scanFrequency + " * * *";
    }
    if (scanFrequency == 0) {
      pm2.delete("lidar-power");
      pm2.delete("start_bag_record");
      pm2.delete("stop_bag_record");
    } else {
      if (data && data[0] && data[0]["pm2_env"]) {
        let processStatus = data[0]["pm2_env"].status;
        pm2.restart(powerConfig, function (errPower, apps) {
          // if (errPower) {
          //   return pm2.disconnect();
          // }
        });
        pm2.restart(startRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });

        pm2.restart(stopRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
      } else {
        //還沒有執行過
        console.log("Lidar has not yet been started");
        pm2.start(powerConfig, function (errPower, apps) {
          // if (errPower) {
          //   return pm2.disconnect();
          // }
        });
        pm2.start(startRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
        pm2.start(stopRecordConfig, function (err, apps) {
          // if (err) {
          //   return pm2.disconnect();
          // }
        });
      }
    }
  });
}

//週期掃描
function applyPeriodic(req, res) {
  let jsonMsg = {
    msg: "",
    code: 200,
  };
  let settingJson = req.body.setting;
  let scanFrequency = settingJson.frequency.value;
  let scanTime = settingJson.duration.value;
  let scanPeriodFilename = settingJson.scanPeriodFilename;

  db.serialize(() => {
    const stmt = db.prepare(
      "UPDATE SETTING SET SCAN_FREQUENCY = ?,SCAN_TIME=?, SCAN_PERIOD_FILENAME=? WHERE ID =1;"
    );
    stmt.run(scanFrequency, scanTime,scanPeriodFilename);
    stmt.finalize();
    restartAllPm2Process(scanFrequency, scanTime, "period",scanPeriodFilename);
    jsonMsg.msg = "更新成功";
    res.send(jsonMsg);
  });
  
}

module.exports = {
  getSetting,
  applyPeriodic,
};

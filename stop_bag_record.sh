#!/bin/sh
echo "scan time: $1"
echo "wait for the machine to start"
sleep 10
echo "wait for the data to be collected"
sleep $1
# rosnode kill /record_*
pm2 stop start_bag_record


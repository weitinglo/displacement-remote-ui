#!/bin/sh
echo "Start Bag Record"
echo "scan type: $1"
echo "instant lidar scan name: $2"
echo "period lidar scan prefix_name: $3"

sleep 10
if [ $1 == "1" ]
then #instant scan
    echo "instant lidar"
    rosbag record -O /home/chiper/FTP/$2.bag -q /livox/lidar
else #period scan  
    echo "period lidar"
    if [ -z "$3" ]
    then
        echo "no prefix "
        date+'FORMAT'
        today=`date +%Y-%m-%d-%H-%M-%S`
        rosbag record -O /home/chiper/FTP/$today -q /livox/lidar
    else
        echo "yes prefix "
        rosbag record -o /home/chiper/FTP/$3 -q /livox/lidar
    fi
fi
const express = require("express");
const app = express();
const port = 8080;
const bodyParser = require("body-parser");
const session = require("express-session");
const FileStore = require("session-file-store")(session);
require('dotenv').config();

let basePath = process.env.BASEPATH;
/**************** UI Config ******************/
const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("displacement.db");

/****************  Done UI Config ******************/
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(basePath + "/assets"));
app.use(express.static(basePath + "/view"));
app.use(express.static(basePath + "/"));
app.use(express.static("/home/chiper/FTP/"));

app.use(
  session({
    name: "session-id",
    secret: "chiper-ai-cam",
    store: new FileStore(),
    saveUninitialized: true,
    resave: false,
    cookie: { maxAge: 20 * 60000 }, //20mins
  })
);

/*********************** Done UDP Setup******************/
let checkLogin = function () {
  return true;
};

app.get("/", function (req, res) {
  if (checkLogin(req)) res.sendFile(__dirname + "/view/" + "index.html");
  else res.sendFile(__dirname + "/view/" + "login.html");
});

app.get("/login", function (req, res) {
  res.sendFile(__dirname + "/view/" + "login.html");
});
/*******************  Device *********************/
let deviceController = require("./controller/deviceController.js");

app.post("/scan", function (req, res) {
    deviceController.scan(req, res);
});
app.get("/deviceStatus", function (req, res) {
    deviceController.deviceStatus(req, res);
});

app.post("/sync", function (req, res) {
  deviceController.sync(req, res);
});

app.post("/liveDisplay", function (req, res) {
  deviceController.liveDisplay(req, res);
});

app.get("/syncStatus", function (req, res) {
  deviceController.syncStatus(req, res);
});


/***************** Account ***********/
let accountContoller = require("./controller/accountController.js");

app.put("/updatePassword", function (req, res) {
  accountContoller.updatePassword(req, res);
});

/***************** Setting ***********/
let settingController = require("./controller/settingController.js");

app.get("/setting", function (req, res) {
  settingController.getSetting(req, res);
});


app.post("/applyPeriodic", function(req,res){
  settingController.applyPeriodic(req,res);
})


/*******************  Done Device *********************/

/*******************  Session *********************/
let sessionController = require("./controller/sessionController.js");
app.post("/auth", function (req, res) {
  sessionController.auth(req, res);
});
app.post("/check_login", function (req, res) {
  sessionController.checkLoginRequest(req, res);
});

app.post("/destory_session", function (req, res) {
  sessionController.destorySession(req, res);
});

/*******************  Done Session *********************/

/*******************  Files *********************/
let fileController = require("./controller/fileController.js");
app.get("/files", function (req, res) {
  fileController.listFiles(req, res);
});
app.delete("/files", function (req, res) {
  fileController.deleteFile(req, res);
});
/*******************  Done Files *********************/

/*******************  System *********************/
let systemController = require("./controller/systemController.js");
app.get("/system_storage", function (req, res) {
  systemController.storage(req, res);
});

/*******************  System *********************/

/******************* check default setting *****************/
// setTimeout(deviceController.initSetting, 20000);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));



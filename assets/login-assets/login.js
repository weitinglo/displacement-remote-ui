
function loginSubmit() {
  $("#warning-msg").empty();
  let name = $("#username").val();
  let pass = $("#password").val();
  let errorMsg = '';
  if (!name || !pass) {
    errorMsg = '請輸入使用者名稱和密碼 ';
  }
  if (errorMsg) {
    chiperUtils.showErrorNotification('top', 'center', errorMsg)
    return;
  }
  $.post("/auth",
    {
      name: name,
      pass: pass
    },
    function (msg, status) {
      console.log(status)
      if (msg.code != 200) {
        chiperUtils.showErrorNotification('top', 'center', msg.msg)
      } else {
        goHomePage();
      }
    });
}

var input = document.getElementById("password");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    loginSubmit();
  }
});
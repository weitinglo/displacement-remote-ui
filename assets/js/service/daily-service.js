
app.factory("dailyService", function ($http) {
    return {
        getDaily: function (date) {
            return $http.get('daily?date=' + date)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getDailyHour: function (date, hour) {
            return $http.get('daily_hour?date=' + date + "&hour=" + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getDailyRange: function (startDate,endDate,area) {
            return $http.get('daily_range?startDate=' + startDate + "&endDate=" + endDate + "&area=" + area)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getDailySearch: function (startDate,startHour,endDate,endHour,area) {
            return $http.get('daily_search?startDate=' + startDate + "&startHour=" + startHour  + "&endDate=" + endDate  + "&endHour=" + endHour+ "&area=" + area)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getDailyArea: function (area, hour) {
            return $http.get('daily_area?area=' + area + "&hour=" + hour)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },       
        downloadDailyData: function (date, hour) {
            return $http.post('download_daily_data', {
                'date': date,
                'hour': hour
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getDailyAlert : function () {
            return $http.get('daily_alert')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
       
    }

})


app.factory("settingService", function ($http) {
    return {
        getSetting: function () {
            return $http.get('setting')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        applyPeriodic: function (periodicConfig) {
            return $http.post('applyPeriodic',{
                setting:periodicConfig
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }

})

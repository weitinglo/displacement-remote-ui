app.factory("fileService", function ($http) {
  return {
    getFiles: function (setting) {
      return $http.get("files").then(
        function (response) {
          return response.data;
        },
        function (response) {
          return response;
        }
      );
    },
    deleteFile: function (filename) {
        return $http.delete("files?filename="+filename).then(
          function (response) {
            return response.data;
          },
          function (response) {
            return response;
          }
        );
      },
  };
});

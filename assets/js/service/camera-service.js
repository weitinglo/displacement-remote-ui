
app.factory("cameraService", function ($http) {
    return {
        getCameraList: function () {
            return $http.get('camera_list')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getCameraInfo: function(ip){
            return $http.get('camera_info?ip='+ip)
            .then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        },
        getSnapshot: function(ip){
            return $http.get('snapshot?ip='+ip)
            .then(function (response) {
                return response.data;
            }, function (response) {
                return response;
            });
        },
    }

})

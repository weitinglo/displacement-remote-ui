
app.factory("deviceService", function ($http) {
    return {
        scan: function (setting) {
            return $http.post('scan',{
                setting:setting
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        liveDisplay: function (enable) {
            return $http.post('liveDisplay',{
                enable:enable
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        deviceStatus: function () {
            return $http.get('deviceStatus')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        sync: function () {
            return $http.post('sync')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        syncStatus: function () {
            return $http.get('syncStatus')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }

})

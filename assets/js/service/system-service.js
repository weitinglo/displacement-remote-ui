app.factory("systemService", function ($http) {
    return {
      getSystemStorage: function () {
        return $http.get("system_storage").then(
          function (response) {
            return response.data;
          },
          function (response) {
            return response;
          }
        );
      },
      getAutoCleanup: function () {
        return $http.get("auto_cleanup").then(
          function (response) {
            return response.data;
          },
          function (response) {
            return response;
          }
        );
      },
      updateAutoCleanup: function (autoClean) {
        let val = autoClean==true?1:0;
        return $http.post('update_auto_cleanup', {
          'autoClean': val
         
      })
        .then(function (response) {
            return response.data;
        }, function (response) {
            return response;
        });
       
      },
    };
  });
  
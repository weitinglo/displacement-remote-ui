
app.factory("accountService", function ($http) {
    return {
        updatePassword: function (updatePasswordJson) {
            return $http.put('updatePassword',{updatePasswordJson:updatePasswordJson})
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }

})


app.factory("sessionService", function ($http) {
    return {
        updateSession: function () {
            return $http.post('update_session')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        destorySession: function () {
            return $http.post('destory_session')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getSession: function () {
            return $http.get('get_session_sql')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        checkLogin: function () {
            return $http.post('/check_login')
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
    }
})

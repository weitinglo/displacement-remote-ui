import * as THREE from '../build/three.module.js';
import { TrackballControls } from '../plugins/jsm/controls/TrackballControls.js';
import { PCDLoader } from '../plugins/jsm/loaders/PCDLoader.js';
import Stats from '../plugins/jsm/libs/stats.module.js';


app.controller('pointcloudController', function ($scope, imageService, networkService, cameraService,detectService) {
    updateMenu('pointcloud');
    let container;
    let camera, controls, scene, renderer;
    let canvas_width = $("#home_canvas").width()-60;
    function init() {
      scene = new THREE.Scene();
      scene.background = new THREE.Color( 0x000000 );
      camera = new THREE.PerspectiveCamera( 20, canvas_width/ window.innerHeight, 0.1, 1000 );
      camera.position.set( -50, 10, 20 );
      camera.up.set( 0, 0, 1 );
      scene.add( camera );
      renderer = new THREE.WebGLRenderer( { antialias: true } );
      renderer.setPixelRatio( window.devicePixelRatio );
      renderer.setSize( canvas_width, window.innerHeight );
      document.body.appendChild( renderer.domElement );

      const loader = new PCDLoader();
      

      loader.load( './pcd/area1.pcd', function ( points ) {
        scene.add( points );
        const center = points.geometry.boundingSphere.center;
        points.material.color.setHex(  0xeb3434 );
        points.material.size = 0.2;
        controls.target.set( center.x, center.y, center.z );
        controls.update();
      } );

      loader.load( './pcd/area2.pcd', function ( points ) {
        scene.add( points );
        const center = points.geometry.boundingSphere.center;
        points.material.color.setHex(  0x80eb34 );
        points.material.size = 0.2;
        controls.target.set( center.x, center.y, center.z );
        controls.update();
      } );

      loader.load( './pcd/area3.pcd', function ( points ) {
        scene.add( points );
        const center = points.geometry.boundingSphere.center;
        points.material.color.setHex(  0x3440eb );
        points.material.size = 0.2;
        controls.target.set( center.x, center.y, center.z );
        controls.update();
      } );

      loader.load( './pcd/cloud_start.pcd', function ( points ) {
        scene.add( points );
        const center = points.geometry.boundingSphere.center;
        points.material.color.setHex(  0x94c6ff );
        points.material.size = 0.2;
        controls.target.set( center.x, center.y, center.z );
        controls.update();
      } );
      container = document.createElement( 'div' );
      document.getElementById("home_canvas").appendChild(container);    
      container.appendChild( renderer.domElement );
      controls = new TrackballControls( camera, renderer.domElement );
      controls.rotateSpeed = 2.0;
      controls.zoomSpeed = 0.3;
      controls.panSpeed = 0.2;
      controls.staticMoving = true;
    
      window.addEventListener( 'resize', onWindowResize );
    }

    function onWindowResize() {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize( window.innerWidth, window.innerHeight );
      controls.handleResize();
    }

    function animate() {
      requestAnimationFrame( animate );
      controls.update();
      renderer.render( scene, camera );
    }
   


    init();
    animate();
   
});


app.controller('detectController', function ($scope, $uibModal, detectService) {
    updateMenu('detect');

    $scope.detectTableList = [];
    $scope.tagLangOptions = [
        {
            name: '中文',
            value: 'tw'
        },
        {
            name: 'English',
            value: 'en'
        }];
    $scope.tagLang = '中文';
    $scope.ChiClassNames = ['人', '腳踏車', '轎車', '機車', '飛機', '公車', '火車',
        '貨車', '船', '紅綠燈', '消防栓', '停車再開標誌', '停車收費器', '長椅', '鳥', '貓',
        '狗', '馬', '羊', '牛', '象', '熊', '斑馬', '長頸鹿', '背包', '傘', '手提包', '領帶',
        '公事包', '飛盤', '雙板雪板', '單板雪板', '球', '風箏', '棒球帽', '棒球手套', '滑板',
        '衝浪板', '網球拍', '水壺', '酒杯', '杯子', '叉子', '刀', '湯匙', '碗', '香蕉', '蘋果',
        '三明治', '橘子', '花椰菜', '胡蘿蔔', '熱狗', '披薩', '甜甜圈', '蛋糕', '椅子', '沙發',
        '盆栽', '床', '餐桌', '馬桶', '電視', '筆電', '滑鼠', '遙控器', '鍵盤', '手機', '微波爐',
        '烤箱', '烤麵包機', '水槽', '冰箱', '書', '時鐘', '花瓶', '剪刀', '泰迪熊', '吹風機', '牙刷'];
    $scope.EngClassNames = ['person', 'bicycle', 'car', 'motorbike', 'aeroplane', 'bus', 'train',
        'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat',
        'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
        'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard',
        'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
        'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'sofa',
        'pottedplant', 'bed', 'dining table', 'toilet', 'tv monitor', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave',
        'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'];
    $scope.selectedClass_idx = 0;
    $scope.selectedClass = $scope.ChiClassNames[$scope.selectedClass_idx]
    var convertTime = function (iso_format) {
        var d = new Date(iso_format);
        var wanted_format = d.toLocaleString().replace('T', ' ').replace('Z', '').split('.')[0];
        return wanted_format;
    }


    function getDetect() {
        detectService.getDetect().then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                if(data.data.length !=$scope.detectTableList.length){
                    $scope.detectTableList = data.data;
                    $scope.detectTableList.forEach(function (item) {
                        item.start_time = convertTime(item.start_time);
                        item.end_time = convertTime(item.end_time);
                    });
                    setPagination();
                }
               
            }
        });
    }

    $scope.playVideo = function (vfilename) {
        $uibModal.open({
            ariaLabelledBy: 'playVideo-modal',
            ariaDescribedBy: 'playVideo-modal',
            templateUrl: 'playVideoModal.html',
            controller: 'playVideoModalController',
            controllerAs: '$scope',
            animation: false,
            size: 'lg',
            backdrop: 'static', // cant close
            appendTo: "",
            resolve: {
                items: function () {
                    return {
                        filename: vfilename
                    };
                }
            }
        }).result.catch(function (resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1) throw resp;
        });
    }

    $scope.enlargeImage = function (footage_id) {
        console.log(footage_id)
    }

    $scope.$watch('selectedClass', function (newValue, oldValue) {
        $scope.selectedClass = newValue;
        $scope.selectedClass_idx = $scope.ChiClassNames.indexOf(newValue);
    });

    $scope.$watch('tagLang', function (newValue, oldValue) {
        $scope.selectedtagLang = newValue;
    });

    $scope.saveSettings = function () {
        $scope.restartDetect($scope.selectedClass, $scope.selectedClass_idx, $scope.selectedtagLang);
    }

    $scope.restartDetect = function (selected_class, class_idx, tag_lang) {
        //pm2 restart here
        // chiperUtils.showSuccessNotification('top', 'center', `更新辨識物件為[${$scope.selectedClass}], 標籤語言為[${tag_lang}]`);
        detectService.runDetect(selected_class, class_idx, tag_lang).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                chiperUtils.showSuccessNotification('top', 'center', data.msg)
            }
        });
    }

    /* Pagination */
    var setPagination = function () {
        $scope.totalItems = $scope.detectTableList.length;
        $scope.maxSize = 5;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;
        $scope.loading = false;

        $scope.$watch("currentPage", function () {
            if (angular.isDefined($scope.currentPage) && angular.isDefined($scope.detectTableList)) {
                setPagingData($scope.currentPage);
            }

        });
        function setPagingData(page) {
            var pagedData = $scope.detectTableList.slice(
                (page - 1) * $scope.itemsPerPage,
                page * $scope.itemsPerPage
            );
            $scope.filteredData = pagedData;
        }
    }

    $scope.init = function () {
        getDetect();
    }

    setInterval(getDetect,2000);

    $scope.init();
});


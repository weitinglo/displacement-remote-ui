app.controller('historyController', function ($scope, $location, dailyService) {
    updateMenu('history');
    $scope.dailyTable = [];
    $scope.hours = [];
    $scope.selectedStartDate = null;
    $scope.selectedStartHour ={};
    $scope.selectedEndDate = null;
    $scope.selectedEndHour ={};

    $scope.areas=[];
    $scope.selectedArea ={};
    
    $scope.displayDate= null;
    $scope.todayDate = getTodayDate();

    function getTodayDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        return yyyy+mm+dd;

    }

    $scope.setArea = function () {
        for(let i=1; i < 90; i++){
            let listData = {
                id:i+1,
                label:i,
                value:i
            }
            $scope.areas.push(listData);
        }
    
        $scope.selectedArea = $scope.areas[0];
    
    }

    
    $scope.setHours = function () {
        for(let i=0; i < 24; i++){
            let listData = {
                id:i+1,
                label:i+'點',
                value:i
            }
            $scope.hours.push(listData);
        }
    
        $scope.selectedStartHour = $scope.hours[0];
        $scope.selectedEndHour = $scope.hours[0];
    
    }

    function getDailySearch(startDate,startHour,endDate,endHour,area) {
        dailyService.getDailySearch(startDate,startHour,endDate,endHour,area).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                $scope.dailyTable = data.data;
               
            }
        });
    }
    function getSearchDateFormat(d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear() + ('0' + (searchDate.getMonth() + 1)).slice(-2) + ('0' + searchDate.getDate()).slice(-2);
    }
    function getDisplayDateFormat(d) {
        let searchDate = new Date(d);
        return searchDate.getFullYear()+"/" + ('0' + (searchDate.getMonth() + 1)).slice(-2) + "/"+ ('0' + searchDate.getDate()).slice(-2);
    }

    $scope.download = function(){
        let jsonData = $scope.dailyTable;
        let csv = "";
        if(jsonData.length>0){
            csv = "displacement,area,date,time,temp,humidity,rain\r\n" 
        }
        for(let i in jsonData){
            console.log(jsonData[i]);
            csv += jsonData[i].displacement+",";
            csv += jsonData[i].area+",";
            csv += jsonData[i].date+",";
            csv += jsonData[i].time+",";
            csv += jsonData[i].temp+",";
            csv += jsonData[i].humidity+",";
            csv += jsonData[i].rain+",";
            csv += '\r\n';
        }
        
        var a = document.createElement("a");
        a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csv);
        a.target = '_blank';
        a.download = '達觀社區A2資料.csv';

        document.body.appendChild(a);
        a.click();
         
    }

    // $scope.download = function(id){
    //     let hour = null;
    //     let date = null;
    //     date = getSearchDateFormat($scope.selectedDate);
    //     if(id==1){ //hour
    //        hour = $scope.selectedHour.value;
    //     }
    //     dailyService.downloadDailyData(date,hour).then(function (data) {
    //         if (data.code && data.code != 200) {
    //             chiperUtils.showErrorNotification('top', 'center', data.msg)
    //         } else {
    //             let jsonData = data.data;
    //             let csv = "";
    //             if(jsonData.length>0){
    //                 csv = "displacement,area,date,time,temp,humidity,rain\r\n" 
    //             }
    //             for(let i in jsonData){
    //                 console.log(jsonData[i]);
    //                 csv += jsonData[i].displacement+",";
    //                 csv += jsonData[i].area+",";
    //                 csv += jsonData[i].date+",";
    //                 csv += jsonData[i].time+",";
    //                 csv += jsonData[i].temp+",";
    //                 csv += jsonData[i].humidity+",";
    //                 csv += jsonData[i].rain+",";
    //                 csv += '\r\n';
    //             }
              
    //             var a = document.createElement("a");
    //             a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csv);
    //             a.target = '_blank';
    //             if(id==1){ //hour
    //                 a.download = '達觀社區A2 '+date+" "+hour+"時資料.csv";
    //             }else{
    //                 a.download = '達觀社區A2 '+date+" 資料.csv";

    //             }
    //             document.body.appendChild(a);
    //             a.click();
    //         }
    //     });
    // }
    $scope.trend = function(area){
        console.log($scope.selectedHour);
        if($scope.selectedHour && $scope.selectedHour.id!=0){
            let h = $scope.selectedHour.value;
            if(h<10)
                h='0'+h;
            window.open("#!/trend/"+area+"/"+$scope.todayDate+"/"+h, '_blank'); 

        }else{
            chiperUtils.showErrorNotification('top', 'center', '請選擇小時')
        }
    }

    $scope.search = function () {
        if ($scope.selectedStartHour == null || $scope.selectedEndHour == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧時間範圍");
            return;
        }
        if ($scope.selectedStartDate == null || $scope.selectedEndDate == null) {
            chiperUtils.showErrorNotification('top', 'center', "請選擇回顧日期範圍");
            return;
        }
        $scope.loading = true;
        let startDate = getSearchDateFormat($scope.selectedStartDate);
        let endDate = getSearchDateFormat($scope.selectedEndDate);

        $scope.displayDate = getDisplayDateFormat($scope.selectedStartDate);
        let startHour = $scope.selectedStartHour.value;
        let endHour = $scope.selectedEndHour.value;

        let area = $scope.selectedArea.value;
        getDailySearch(startDate,startHour,endDate,endHour,area);
    }



    $scope.setHours();
    $scope.setArea();

});


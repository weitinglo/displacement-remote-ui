app.controller('hourlyController', function ($scope, dailyService,$location) {
    updateMenu('hourly');
    $scope.dailyTable = [];
    function getTodayDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        return yyyy+mm+dd;

    }

    function getCurrentHour(offset){
        var today = new Date();
        return today.getHours()-offset;
    }

    function getPreviousHour(offset){
        var today = new Date();
        return today.getHours()-1-offset;
    }

    function calculateHour(todayDate,preHour,offset) {
        dailyService.getDailyHour(todayDate,preHour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {     
                let preHourData = data.data;
                let currentHour = getCurrentHour(offset);
                dailyService.getDailyHour(todayDate,currentHour).then(function (curdataRes) {
                    if (curdataRes.code && curdataRes.code != 200) {
                        chiperUtils.showErrorNotification('top', 'center', curdataRes.msg)
                    } else {     
                        let curHourData = curdataRes.data;
                        if(curHourData.length>0){
                            for(let i in curHourData){
                                let currentHourData = curHourData[i];
                                for(let j in preHourData){
                                    let previousHourData = preHourData[j];       
                                    if(currentHourData.area == previousHourData.area){
                                        let displacement = Math.round((currentHourData.displacement - previousHourData.displacement) * 100) / 100
                                        currentHourData.displacement = displacement;
                                        currentHourData.absDisplacement = Math.abs(displacement);
    
                                        $scope.dailyTable.push(curHourData[i]);
                                    }
                                }
                            }
                            for(let i in $scope.dailyTable){
                                $scope.dailyTable[i].absDisplacementVal = Math.abs($scope.dailyTable[i].displacement);
                                let displacementVal = $scope.dailyTable[i].displacement;
                                if(displacementVal<0 && displacementVal>-0.2){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," green");
                                }else if(displacementVal<=-0.2 && displacementVal>=-0.4){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," yellow");
                                }else if(displacementVal<-0.4){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," red");
                                }else if(displacementVal>0 && displacementVal<0.2){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," #08f0ea");
                                }else if(displacementVal>=0.2 && displacementVal<=0.4){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," rgb(68,54,250)");
                                }
                                else if(displacementVal>0.4){
                                    let area = $scope.dailyTable[i].area;
                                    $("#area_"+area).css("fill"," rgb(199,69,235)");
                                }
                            }   
                        }else if(offset==0){
                            calculateHour( getTodayDate(),getPreviousHour(1),1);
                        }
                                
                    }
                });
            }
        });
    }

   

    // $scope.trend = function(area){
    //     if($scope.selectedHour && $scope.selectedHour.id!=0){
    //         let h = $scope.selectedHour.value;
    //         if(h<10)
    //             h='0'+h;
    //         window.open("#!/trend/"+area+"/"+$scope.todayDate+"/"+h, '_blank'); 

    //     }else{
    //         chiperUtils.showErrorNotification('top', 'center', '請選擇小時')
    //     }
    // }

    $scope.init = function () {
        $scope.todayDate = getTodayDate();
        calculateHour( getTodayDate(),getPreviousHour(0),0);
    }

    $scope.init();
});

app.controller(
  "homeController",
  function ($scope, sessionService, deviceService,settingService,fileService, $window) {
    updateMenu("home");
    $scope.deviceStatus = "連線中"; //目前光達設備狀態
    $scope.syncStatus = "連線中"; //目前資料同步狀態
    $scope.syncDateimte = ""; //最後同步時間
    let nowDate = new Date();
    let currentDatetime = nowDate.getFullYear()+"-"+String(nowDate.getMonth()+1).padStart(2,'0')+"-"+String(nowDate.getDate()).padStart(2,'0')+"-"+String(nowDate.getHours()).padStart(2,'0')+"-"+String(nowDate.getMinutes()).padStart(2,'0')+"-"+String(nowDate.getSeconds()).padStart(2,'0')

    $scope.lidarFilename = currentDatetime;
    $scope.lidarPeriodFilename = null;
    $scope.deviceAbleToRun = true; //是否可以執行光達掃描，判斷是否有其他功能使用中
    $scope.deviceNSyncAbleToRun = true; //是否可以執行光達掃描，判斷是否正在進行資料同步
    $scope.isDeviceStreaming = false;
    $scope.fileList = [];
    $scope.needGetFiles = false;
    $scope.scantimeFreqOptions = [
      { id: 1, value: "15", label: "極速掃描-15秒" },
      { id: 2, value: "30", label: "快速掃描-30秒" },
      { id: 3, value: "60", label: "一般掃描-60秒" },
      { id: 4, value: "90", label: "深層掃描-90s"},
      { id: 5, value: "120",label: "高解析掃描-120s"},
    ];

    $scope.periodicOptions = [
      { id: 1, value: "0", label: "無" },
      { id: 2, value: "1", label: "每小時掃描一次" },
      { id: 3, value: "2", label: "每2小時掃描一次" },
      { id: 4, value: "6", label: "每6小時掃描一次" },
      { id: 5, value: "12", label: "每12小時掃描一次" },
      { id: 5, value: "24", label: "每24小時掃描一次" },
    ];
    $scope.selectedPeriod = $scope.periodicOptions[1];
    $scope.selectedPeriodScantime =  $scope.scantimeFreqOptions[0];
    $scope.selectedScantime = $scope.scantimeFreqOptions[0];
    let checkLogin = function () {
      sessionService.checkLogin().then(function (data) {
        if (data.code != 200) {
          window.location.href = "/login.html";
        }
      });
    };

    //即時光達畫面
    $scope.liveDisplay = function(enable){
      deviceService.liveDisplay(enable).then(function (data) {
        deviceStatus();
        getSetting();
      });
    }

    //即時掃描
    $scope.scan = function () {
      let setting = {
        scantime: $scope.selectedScantime.value,
        lidarFilename:$scope.lidarFilename
      };
    
      deviceService.scan(setting).then(function (data) {
        deviceStatus();
        getSetting();
      });
    };

    const deviceStatus = function () {
      deviceService.deviceStatus().then(function (data) {
        $scope.isDeviceStreaming = false;
        if (data.status == "stopped" || data.status == "") { //光達關機
          $scope.deviceStatus = "待機中";
          $scope.deviceAbleToRun = true;
          if($scope.deviceNSyncAbleToRun){
            $(".card").css("background","#27293d")
          }
          $(".device-status").css("color", "white");
	  if($scope.needGetFiles){
	    $scope.needGetFiles = false;
	    $scope.getFiles();
	  }
        } else if (data.status == "online") {  //光達開機
          $scope.deviceStatus = "掃描運行中"; 
	  $scope.needGetFiles = true;
          $scope.deviceAbleToRun = false;
          if($scope.deviceNSyncAbleToRun)
            $(".card").css("background","#573109")
          $(".device-status").css("color", "orange");
        } else if (data.status == "waiting restart" || data.msg == 'Live Stream 運行中') { //即時畫面開啟
          $scope.isDeviceStreaming = true;
          $scope.deviceAbleToRun = false;
          if($scope.deviceNSyncAbleToRun)
            $(".card").css("background","#573109")
          $(".device-status").css("color", "orange");
        } else if (data.status == "error") {
          $scope.deviceStatus = "系統錯誤";
          $scope.deviceAbleToRun = false;
          if($scope.deviceNSyncAbleToRun)
            $(".card").css("background","#590101")
          $(".device-status").css("color", "red");
        } 
      });
    };

    $scope.sync = function () {
      deviceService.sync().then(function (data) {
        deviceStatus();
      });
    };

    const syncStatus = function () {
      deviceService.syncStatus().then(function (data) {
        $scope.syncDateimte = data.sync_datetime;
        if (data.status == "stopped" || data.status == "") {
          $scope.syncStatus = "---";
          $scope.deviceNSyncAbleToRun = true;
          if($scope.deviceAbleToRun){
            $(".card").css("background","#27293d")
          }
        } else if (data.status == "online") {
          $scope.syncStatus = "同步中";
          $scope.deviceNSyncAbleToRun = false;
          if($scope.deviceAbleToRun){
            $(".card").css("background","#5059ae")
          }
        } else if (data.status == "error") {
          $scope.syncStatus = "系統錯誤";
          $scope.deviceNSyncAbleToRun = false;
          if($scope.deviceAbleToRun){
            $(".card").css("background","#5059ae")
          }

        }
      });
    };

    //週期掃描
    $scope.applyPeriodic = function(){
      let periodicConfig = {
        frequency:$scope.selectedPeriod,
        duration:$scope.selectedPeriodScantime,
        scanPeriodFilename:$scope.lidarPeriodFilename
      }
      $scope.deviceAbleToRun = false;
      settingService.applyPeriodic(periodicConfig).then(function (data) {
        if(data.code==200){
          chiperUtils.showNotification('top', 'center', data.msg);

        }else{
          chiperUtils.showErrorNotification('top', 'center', data.msg);

        }
      });
    }

    let getSetting = function(){
      settingService.getSetting().then(function (data) {
        if(data.code==200){
          let scan_frequency = data.data.scan_frequency;
          let scan_time = data.data.scan_time;
          if(data.data.SCAN_PERIOD_FILENAME){
            $scope.lidarPeriodFilename = data.data.SCAN_PERIOD_FILENAME;
          }
          for(let i in $scope.periodicOptions){
            if($scope.periodicOptions[i].value == scan_frequency){
              $scope.selectedPeriod = $scope.periodicOptions[i];
            }
          }
          for(let i in $scope.scantimeFreqOptions){
            if($scope.scantimeFreqOptions[i].value == scan_time){
              $scope.selectedPeriodScantime = $scope.scantimeFreqOptions[i];
            }
          }
          

        }
      });
    }

    $scope.getFiles = function(){
      
      fileService.getFiles().then(function (data) {
        if(data.code==200){
          $scope.fileList = [];
          if(data && data.data){
            let files = data.data;
            for(let i in files){
              if(!files[i].name.includes(".sh")){
                let fileSizeInMB = files[i].fileSizeInBytes/1000000;
                files[i].fileSizeInMB = fileSizeInMB.toFixed(1);
                $scope.fileList.push(files[i]);
              }
            }
          }          
        }
      });
    }
    $scope.deleteAllFile = async function(){
	for(let i=0;i<$scope.fileList.length; i++){
		await fileService.deleteFile($scope.fileList[i].name);
	}
	$scope.getFiles();
    }
    //刪除檔案
    $scope.deleteFile = function(filename){
      fileService.deleteFile(filename).then(function(data){
        if(data.code==200){
          chiperUtils.showNotification('top', 'center', data.msg);
          $scope.getFiles();
        }else{
          chiperUtils.showErrorNotification('top', 'center', data.msg);

        }
      })
    }

    let init = function () {
      // checkLogin();
      // deviceStatus();
      // syncStatus();
      getSetting();
      $scope.getFiles();
    };

    init();
    var deviceStatusInterval = setInterval(deviceStatus, 2000);
    var syncStatusInterval = setInterval(syncStatus, 2000);

    $scope.$on("$locationChangeStart", function (event, next, current) {
      clearInterval(deviceStatusInterval);
      clearInterval(syncStatusInterval);
    });
  }
);

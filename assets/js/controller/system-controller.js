app.controller("systemController", function ($scope, systemService) {
  updateMenu("system");
  $scope.currentTab=1;
  $scope.auto3MonthClean = true;

  $scope.systemStorage={
    total:0,
    free:0,
    percent:0
  }

  $scope.getSystemStorage = function () {
    systemService.getSystemStorage().then(function (res) {
      if (res.code && res.code != 200) {
        chiperUtils.showErrorNotification("top", "center", res.msg);
      } else {
        $scope.systemStorage.total = (res.data.totalStorage/1000000000).toFixed(1);
        $scope.systemStorage.free = (res.data.freeStorage/1000000000).toFixed(1);
        $scope.systemStorage.percent = ((res.data.freeStorage*100)/res.data.totalStorage).toFixed(1);
        if($scope.systemStorage.percent<50){
          $("#progressBar").css("background-color","orange");
        }else if($scope.systemStorage.percent<50){
          $("#progressBar").css("background-color","red");
        }
        $("#progressBar").css("width",$scope.systemStorage.percent+"%");
      }
    });
  };
  $scope.changeTab = function(tab){
    $scope.currentTab=tab;
   
  }

  $scope.init = function(){
    $scope.getSystemStorage();
  }
  $scope.init();
});

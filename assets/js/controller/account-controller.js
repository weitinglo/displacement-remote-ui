app.controller(
  "accountController",
  function ($scope, accountService, deviceService, $window) {
    updateMenu("account");
    $scope.account = "admin";
    $scope.currentPassword="";
    $scope.newPassword = "";
    $scope.newPasswordConfrim = "";

    $scope.updatePassword = function(){
      if(!$scope.account || !$scope.currentPassword || !$scope.newPassword || !$scope.newPasswordConfrim){
        chiperUtils.showErrorNotification('top', 'center', "請輸入所有需要資訊");
        return;
      }

      if($scope.newPasswordConfrim!=$scope.newPassword){
        chiperUtils.showErrorNotification('top', 'center', "新密碼與新密碼確認不一致");
        return;
      }
      let updatePasswordJson = {
        account:"admin",
        currentPassword:$scope.currentPassword,
        newPassword:$scope.newPassword
      }
      accountService.updatePassword(updatePasswordJson).then(function (data) {
        if(data.code==200){
          chiperUtils.showNotification('top', 'center', data.msg);

        }else{
          chiperUtils.showErrorNotification('top', 'center', data.msg);

        }
      });

    }
  }
);

app.controller('dailyController', function ($scope, dailyService,$location) {
    updateMenu('daily');

    $scope.dailyTable = [];
    $scope.dailyTableFull = [];
    $scope.hours=[];
    $scope.selectedHour ={};
    $scope.hours.push({
        id:0,
        label:'顯示全部',
        value:'all'
    });
    $scope.setHours = function () {
        for(let i=0; i < 24; i++){
            let listData = {
                id:i+1,
                label:i+'點',
                value:i
            }
            $scope.hours.push(listData);
        }
        $scope.selectedHour = $scope.hours[1];
    }
    
    $scope.filterHour = function(){
        $scope.dailyTable = [];
        for(i in $scope.dailyTableFull){
            if($scope.dailyTableFull[i].time==$scope.selectedHour.value){
                $scope.dailyTable.push($scope.dailyTableFull[i]);
            }
        }
    }

    function getDaily(todayDate) {
        dailyService.getDailyHour(todayDate,0).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                $scope.dailyTable = data.data;
                $scope.dailyTableFull = data.data;    
                for(let i in $scope.dailyTable){
                    $scope.dailyTable[i].absDisplacementVal = Math.abs($scope.dailyTable[i].displacement);
                    let displacementVal = $scope.dailyTable[i].displacement;
                    if(displacementVal<0 && displacementVal>-1){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," green");
                    }else if(displacementVal<=-1 && displacementVal>=-2){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," yellow");
                    }else if(displacementVal<-2){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," red");
                    }else if(displacementVal>0 && displacementVal<1){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," #08f0ea");
                    }else if(displacementVal>=1 && displacementVal<=2){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," rgb(68,54,250)");
                    }
                    else if(displacementVal>2){
                        let area = $scope.dailyTable[i].area;
                        $("#area_"+area).css("fill"," rgb(199,69,235)");
                    }
                    console.log(displacementVal);
                }           
            }
        });
    }

    function getTodayDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        return yyyy+mm+dd;

    }

    $scope.trend = function(area){
        console.log($scope.selectedHour);
        if($scope.selectedHour && $scope.selectedHour.id!=0){
            let h = $scope.selectedHour.value;
            if(h<10)
                h='0'+h;
            window.open("#!/trend/"+area+"/"+$scope.todayDate+"/"+h, '_blank'); 

        }else{
            chiperUtils.showErrorNotification('top', 'center', '請選擇小時')
        }
    }

    $scope.init = function () {
        $scope.setHours();
        $scope.todayDate = getTodayDate();
        let todayDate = getTodayDate();
        getDaily(todayDate);
    }

    $scope.init();
});

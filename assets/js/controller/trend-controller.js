app.controller('trendController', function ($scope,$routeParams,dailyService) {
    $scope.area = $routeParams.area;
    $scope.hour = $routeParams.hour;
    $scope.date = $routeParams.date;
    $scope.trendTable = [];
    $scope.trendDate = [];
    $scope.trendDisplacement = [];
    $scope.init=function(){
        dailyService.getDailyArea($scope.area,$scope.hour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {
                $scope.trendTable = data.data;
                for(i in $scope.trendTable){
                    let dateVal = $scope.trendTable[i].date;
                    $scope.trendDate.push(dateVal);
                    let displacementVal = $scope.trendTable[i].displacement;
                    $scope.trendDisplacement.push(displacementVal);
                }
                let myConfig = {
                    type: 'line',
                    backgroundColor: '#2C2C39',
                    title: {
                      text: '區塊 '+$scope.area+' 趨勢 - '+$scope.hour+'時',
                      fontSize: 24,
                      fontColor: '#E3E3E5'
                    },
                    legend: {
                      draggable: true,
                    },
                    scaleX: {
                      label: { text: '日期',fontColor: '#ffffff' },
                      labels:  $scope.trendDate,
                      item: {
                        fontColor: '#ffffff'
                      }
                    },
                    scaleY: {
                      label: { text: '變位量 (cm)',fontColor: '#ffffff' },
                      item: {
                        fontColor: '#ffffff'
                      }
                    },
                    plot: {
                      animation: {
                        effect: 'ANIMATION_EXPAND_BOTTOM',
                        method: 'ANIMATION_STRONG_EASE_OUT',
                        sequence: 'ANIMATION_BY_NODE',
                        speed: 275,
                      }
                    },
                    plotarea: {
                      marginLeft: '3%',
                      marginRight: '3%',
          
                    },
                    series: [
                      {
                        values: $scope.trendDisplacement,
                        text:'區塊 '+$scope.area+' 趨勢 - '+$scope.hour+'時 每日變位量',
                      }
                    ]
                  };
                
                zingchart.render({
                    id: 'myChart',
                    data: myConfig,
                  });
              
            }
        });
    }
    $scope.init();
   
});


app.controller('logoutController', function ($scope, sessionService) {
    sessionService.destorySession().then(function (data) {
        goLoginPage();
    });
});


app.controller('trend24hController', function ($scope, $routeParams, dailyService) {
  $scope.area = $routeParams.area;
  $scope.hour = $routeParams.hour;

  let paramDate = $routeParams.date;
  let endDate = new Date(paramDate.substring(0, 4) + '/' + paramDate.substring(4, 6) + '/' + paramDate.substring(6, 8));
  let startDate = new Date();
  startDate.setDate(endDate.getDate() - 1);

  $scope.startDate = dateFormat(startDate);
  $scope.endDate = dateFormat(endDate);

  $scope.trendTable = [];
  $scope.trendDate = [];
  $scope.trendDisplacement = [];
  $scope.trendRain = [];
  $scope.init = function () {
    dailyService.getDailyRange($scope.startDate, $scope.endDate, $scope.area).then(function (data) {
      if (data.code && data.code != 200) {
        chiperUtils.showErrorNotification('top', 'center', data.msg)
      } else {
        let resData = data.data;

        for (i in resData) {
          let dateVal = resData[i].date;
          let timeVal = resData[i].time;
          if (dateVal == $scope.startDate && timeVal >= $scope.hour) {
            $scope.trendDate.push(dateVal + " " + timeVal + "時");
            let displacementVal = resData[i].displacement;
            let rainVal = resData[i].rain == null ? 0 : parseFloat(resData[i].rain);
            $scope.trendDisplacement.push(displacementVal);
            $scope.trendRain.push(rainVal);
            $scope.trendTable.push(resData[i]);

          } else if (dateVal == $scope.endDate && timeVal <= $scope.hour) {
            $scope.trendDate.push(dateVal + " " + timeVal + "時");
            let displacementVal = resData[i].displacement;
            let rainVal = resData[i].rain == null ? 0 : parseFloat(resData[i].rain);
            $scope.trendDisplacement.push(displacementVal);
            $scope.trendRain.push(rainVal);
            $scope.trendTable.push(resData[i]);
          }

        }
        let myConfig = {
          type: 'mixed',
          backgroundColor: '#2C2C39',
          title: {
            text: '區塊 ' + $scope.area + ' 趨勢 - 過去24小時變位量',
            fontSize: 24,
            fontColor: '#E3E3E5',
          },
          legend: {
            draggable: true,
          },
          scaleX: {
            label: { text: '日期', fontColor: '#ffffff' },
            labels: $scope.trendDate,
            item: {
              fontColor: '#ffffff'
            }
          },
          scaleY: {
            label: { text: '變位量 (cm)', fontColor: '#ffffff' },
            item: {
              fontColor: '#ffffff'
            }
          },
          scaleY2: {
            label: { text: '降雨量 (mm)', fontColor: '#ffffff' },
            item: {
              fontColor: '#ffffff'
            },
            step:0.5 // can define scale step values or default
          },
          legend: {
            adjustLayout: true,
            verticalAlign: 'middle'
          },
          plot: {
            animation: {
              effect: 'ANIMATION_EXPAND_BOTTOM',
              method: 'ANIMATION_STRONG_EASE_OUT',
              sequence: 'ANIMATION_BY_NODE',
              speed: 275,
            }
          },
          plotarea: {
            marginLeft: '3%',
            marginRight: '3%',

          },
          crosshairX: {
            lineColor: '#ffffff',
            lineGapSize: '4px',
            lineStyle: 'dotted',
            plotLabel: {
              padding: '15px',
              backgroundColor: 'white',
              bold: true,
              borderColor: '#e3e3e3',
              borderRadius: '5px',
              fontColor: '#2f2f2f',
              fontFamily: 'Lato',
              fontSize: '12px',
              shadow: true,
              shadowAlpha: 0.2,
              shadowBlur: 5,
              shadowColor: '#a1a1a1',
              shadowDistance: 4,
              textAlign: 'left'
            },
            scaleLabel: {
              padding: '15px',
              backgroundColor: 'white',
              fontColor:'black',
              fontSize: '16px',
              bold: true,
              borderColor: '#e3e3e3',
              borderRadius: '5px',
            }
          },
          series: [
            {
              type: "line",
              values: $scope.trendDisplacement,
              text: '變位量',
              scales: 'scale-x, scale-y'
            },
            {
              type: "bar",
              values: $scope.trendRain,
              text: '降雨量',
              scales: 'scale-x, scale-y-2'
            }
          ]
        };

        zingchart.render({
          id: 'myChart',
          data: myConfig
        });

      }
    });
  }
  $scope.init();

});


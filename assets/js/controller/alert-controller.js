app.controller('alertController', function ($scope, dailyService) {
    updateMenu('alert');
    $scope.dailyTable = [];
    function getTodayDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        return yyyy+mm+dd;

    }

    function getCurrentHour(offset){
        var today = new Date();
        return today.getHours()-offset;
    }

    function getPreviousHour(offset){
        var today = new Date();
        return today.getHours()-1-offset;
    }

    function calculateHour(todayDate,preHour,offset) {
        dailyService.getDailyHour(todayDate,preHour).then(function (data) {
            if (data.code && data.code != 200) {
                chiperUtils.showErrorNotification('top', 'center', data.msg)
            } else {     
                let preHourData = data.data;
                let currentHour = getCurrentHour(offset);
                dailyService.getDailyHour(todayDate,currentHour).then(function (curdataRes) {
                    if (curdataRes.code && curdataRes.code != 200) {
                        chiperUtils.showErrorNotification('top', 'center', curdataRes.msg)
                    } else {     
                        let curHourData = curdataRes.data;
                        if(curHourData.length>0){
                            for(let i in curHourData){
                                let currentHourData = curHourData[i];
                                for(let j in preHourData){
                                    let previousHourData = preHourData[j];       
                                    if(currentHourData.area == previousHourData.area){
                                        let displacement = Math.round((currentHourData.displacement - previousHourData.displacement) * 100) / 100;
                                        if(Math.abs(displacement)>0.5){
                                            currentHourData.displacement = displacement;
                                            currentHourData.absDisplacement = Math.abs(displacement);
                                            $scope.dailyTable.push(curHourData[i]);
                                        }
                                        
                                    }
                                }
                            }
                        }else if(offset==0){
                            calculateHour( getTodayDate(),getPreviousHour(1),1);
                        }    
                    }
                });
            }
        });
    }

    $scope.init = function () {
        $scope.todayDate = getTodayDate();
        calculateHour( getTodayDate(),getPreviousHour(0),0);
    }

    $scope.init();
});


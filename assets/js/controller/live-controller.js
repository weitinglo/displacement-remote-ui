app.controller("liveController", function ($scope, sessionService, deviceService, settingService, fileService, $window) {
  updateMenu("live");
  container = document.getElementById("viewer");
  document.body.appendChild(container);
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(15, window.innerWidth / window.innerHeight, 0.01, 40);
  camera.position.x = -15;
  camera.position.y = 0;
  camera.position.z = 0;
  camera.up.set(0, 0, 1);
  camera.lookAt(new THREE.Vector3(0, 0, 0));
  controls = new THREE.OrbitControls(camera);

  controls.rotateSpeed = 2.0;
  controls.zoomSpeed = 0.3;
  controls.panSpeed = 0.2;

  controls.enableZoom = false;
  controls.enablePan = false;

  controls.enableDamping = true;
  controls.dampingFactor = 0.3;

  controls.minDistance = 0.3;
  controls.maxDistance = 0.3 * 100;
  controls.update();
  controls.addEventListener("change", () => renderer.render(scene, camera));
  scene.add(camera);
  var axisHelper = new THREE.AxisHelper(0.1);
  scene.add(axisHelper);

  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth-265, window.innerHeight-170);
  container.appendChild(renderer.domElement);
  const white_color = new THREE.Color("rgb(255,255,255)");
  function refresh() {
    count= 0;
    
     
      var loader = new THREE.PCDLoader();
      loader.load(
        "../assets/js/plugins/threeJS/pointcloud/data.pcd",
        function (mesh) {
          var selectedObject = scene.getObjectByName("obj_data");
          scene.remove( selectedObject );
          mesh.material.color = white_color;
          mesh.material.size = 0.06;
          mesh.name = "obj_data"
          scene.add(mesh);
          renderer.render(scene, camera);
         
          setTimeout(refresh,1000);
          
        },
        (xhr) => {
        }
      );
    
  }

  refresh();
//  setInterval(refresh,6000)
});

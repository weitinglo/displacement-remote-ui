function updateMenu(menu) {
  $("#menuHome").removeClass("active");
  $("#menuAccount").removeClass("active");
  $("#menuSystem").removeClass("active");
  $("#menuLive").removeClass("active");

  switch (menu) {
    case "home":
      $("#menuHome").addClass("active");
      break;
    case "live":
      $("#menuLive").addClass("active");
      break;
    case "account":
      $("#menuAccount").addClass("active");
      break;
    case "system":
      $("#menuSystem").addClass("active");
      break;
  }
}

addDateZero = function (value) {
  var str = "" + value;
  while (str.length < 2) {
    str = "0" + str;
  }
  return str;
};

function dateFormat(date) {
  return (
    date.getFullYear() +
    "" +
    addDateZero(date.getMonth() + 1) +
    "" +
    addDateZero(date.getDate())
  );
}
function goHomePage() {
  window.location.href = window.location.protocol + "//" + window.location.host + "/#!/home";
}

function goLoginPage() {
  window.location.href =
    window.location.protocol + "//" + window.location.host + "/login.html";
}

var chiperUtils = {
  misc: {
    navbar_menu_visible: 0,
  },

  showNotification: function (from, align, msg) {
    color = "info";
    $.notify(
      {
        icon: "tim-icons icon-bell-55",
        message: msg,
      },
      {
        type: color,
        timer: 3000,
        placement: {
          from: from,
          align: align,
        },
      }
    );
  },
  showErrorNotification: function (from, align, msg) {
    color = "danger";
    $.notify(
      {
        icon: "tim-icons icon-alert-circle-exc",
        message: msg,
      },
      {
        type: color,
        timer: 5000,
        placement: {
          from: from,
          align: align,
        },
      }
    );
  },
  showSuccessNotification: function (from, align, msg) {
    color = "success";
    $.notify(
      {
        icon: "tim-icons icon-check-2",
        message: msg,
      },
      {
        type: color,
        timer: 3000,
        placement: {
          from: from,
          align: align,
        },
      }
    );
  },
};

$(document).on("click", ".navbar-toggle", function () {
  ($toggle = $(this)),
    1 == chiperUtils.misc.navbar_menu_visible
      ? ($("html").removeClass("nav-open"),
        (chiperUtils.misc.navbar_menu_visible = 0),
        setTimeout(function () {
          $toggle.removeClass("toggled"), $("#bodyClick").remove();
        }, 550))
      : (setTimeout(function () {
          $toggle.addClass("toggled");
        }, 580),
        (div = '<div id="bodyClick"></div>'),
        $(div)
          .appendTo("body")
          .click(function () {
            $("html").removeClass("nav-open"),
              (chiperUtils.misc.navbar_menu_visible = 0),
              setTimeout(function () {
                $toggle.removeClass("toggled"), $("#bodyClick").remove();
              }, 550);
          }),
        $("html").addClass("nav-open"),
        (chiperUtils.misc.navbar_menu_visible = 1));
});

app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: '/home.htm',
        controller: 'homeController'
      })  
      .when('/live', {
        templateUrl: '/live.htm',
        controller: 'liveController'
      })  
      .when('/account', {
        templateUrl: '/account.htm',
        controller: 'accountController'
      })  
      .when('/system', {
        templateUrl: '/system.htm',
        controller: 'systemController'
      })  
      .when('/logout', {
        templateUrl: '/logout.htm',
        controller: 'logoutController'
      })
      .otherwise({
        redirectTo: '/logout'
      });
  }]);
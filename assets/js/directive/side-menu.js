app.directive("sideMenu", function () {
  return {
    template:
      ' <div class="sidebar" data="green">' +
      '<div class="sidebar-wrapper">' +
      ' <div class="logo">' +
      '<a href="javascript:void(0)" class="simple-text logo-mini">' +
      '<img src="./img/asap-logo.png" alt="Italian Trulli">>' +
      ' </a>' +
      // '<a href="javascript:void(0)" class="simple-text logo-normal" style="padding-top: 16px;">' +
      // '變位監測' +
      // '</a>' +
      '</div>' +
      '<ul class="nav">' +
     
      '<li id="menuHome">' +
      '<a href="#!home">' +
      ' <i class="tim-icons icon-sound-wave"></i>' +
      '<p>變位掃描</p>' +
      ' </a>' +
      '</li>' +
      // '<li id="menuLive">' +
      // '<a href="#!live">' +
      // ' <i class="tim-icons icon-video-66"></i>' +
      // '<p>即時畫面</p>' +
      // ' </a>' +
      // '</li>' +
      '<li id="menuSystem">' +
      '<a href="#!system">' +
      ' <i class="tim-icons icon-settings-gear-63"></i>' +
      "<p>系統管理</p>" +
      " </a>" +
      "</li>" +
      '<li id="menuAccount">' +
      '<a href="#!account">' +
      ' <i class="tim-icons icon-badge"></i>' +
      '<p>帳號管理</p>' +
      ' </a>' +
      '</li>' +
      '<li id="menuLogout">' +
      '<a href="#!logout">' +
      ' <i class="tim-icons icon-button-power"></i>' +
      '<p>平台登出</p>' +
      ' </a>' +
      '</li>' +
      ' </ul>' +
      '</div>' +
      '</div>'
  };
});
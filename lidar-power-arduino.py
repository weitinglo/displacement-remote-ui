#!/usr/bin/env python
import time
import sys
import serial
import os
COM_PORT = "/dev/ttyACM0"
BAUD_RATES = 115200

print ('Number of arguments:', len(sys.argv), 'arguments.')
print(sys.argv[1])

try:
    sleepTime = int(sys.argv[1])
    ser = serial.Serial(COM_PORT,BAUD_RATES,timeout=2)
    time.sleep(2)
    ser.write(b'1\n')
    time.sleep(sleepTime+10)
    ser.write(b'0\n')

except Exception as e:
    print(e)

      

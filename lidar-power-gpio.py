#!/usr/bin/env python
import time
import sys
import RPi.GPIO as GPIO
import os

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)


#print ('Number of arguments:', len(sys.argv), 'arguments.')
#print(sys.argv[1])

try:
    sleepTime = int(sys.argv[1])
    print(sleepTime)
    GPIO.output(18, GPIO.HIGH)
    time.sleep(sleepTime+10)
    GPIO.output(18, GPIO.LOW)

except Exception as e:
    print(e)

      
